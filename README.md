# Cryptocurrency volatility calculation service

## Requirements
- postgresql
- python 3.6

pip packages can be found in `dev/requirements.txt`

## Setup
First we need to configure the postgres user and database. To do this, connect to postgres as a superuser and run

```
CREATE ROLE <username> WITH LOGIN PASSWORD '<passwd>';
CREATE DATABASE <db_name>;
GRANT ALL ON DATABASE <db_name> TO <username>;
```

In order to run the tests, we need to repeat this with another user and db, if desired, though the server can be configured to use the primary database for tests also. The server uses the following defaults

```
PGUSER = os.environ.get('PGUSER') or 'lb'
PGPASSWD = os.environ.get('PGPASSWD') or 'lb'
PGHOST = os.environ.get('PGHOST') or '127.0.0.1'
PGPORT = os.environ.get('PGPORT') or 5432
DBNAME = os.environ.get('DBNAME') or 'cryptocompare'
```
and for the test suite,

```
PGUSER_TEST = os.environ.get('PGUSER_TEST') or 'lb_test'
PGPASSWD_TEST = os.environ.get('PGPASSWD_TEST') or 'lb_test'
DBNAME_TEST = os.environ.get('DBNAME_TEST') or 'lb_test'
```
and so can be configured with the above environment variables, or run with the defaults.

## Running the server
The server is started by running `serve.py`. By default it will server on port 8000 but this can be configured with the environment variable `VOL_PORT`.


## API
The server exposes an endpoint at `/volatility` which takes `get` requests with parameters `principal` and `collateral`. The response format is json with the schema,

```
{
    'principal': string - principal currency,
    'collateral': string - collateral currency,
    'month': float, - standard deviation of daily log returns over one month,
    'three_months': float - as above over three months,
    'year': float - as above over one year,
    'timestamp': float - timestamp at midnight corresponding to the last close price,
    'three_month_inferred': bool,
    'year_inferred': bool,
}
```

The `*_inferred` fields indicate that data could not be obtained for the specified period, and so an approximation was performed by annualising the monthly volatility, or similarly approximating the three month return. We obtain this by multiplying the monthly volatility by sqrt(12) or sqrt(3) respectively.

Data can be missing where a pair was not trading over some period, e.g. in the case of BCH before the fork on the 1st of August 2017.

## Tests
The tests are located in `volatility/tests/` and can be run with `py.test`.

## Future improvements

### Error handling in the background tasks
For example, if we are rate limited or for some other reason fail to reach CryptoCompare then we could reschedule the task with an exponential back-off over a fixed number of attempts. Same goes for possible loss of connection with the DB.

### Startup
For simplicity, I clear the DB on startup and request 365 days of close prices from the API. A more efficient solution would be to write a catch-up which only pulls down missing data and leaves the DB intact between runs. This would be vital for a production server.

## Connect to db without blocking
At the moment, running postgres commands will block the event loop. I could get round this with a package like `aiopg`, or by running with `loop.run_in_executor`. However, considering the relative speed of these simple postgres queries against python and HTTP requests, I doubt we'd see much difference in performance. I'm curious though so I'll do some profiling when I have a chance.
