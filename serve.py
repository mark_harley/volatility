import os

from aiohttp import web

from volatility.server import Server


VOL_PORT = os.environ.get('VOL_PORT') or 8000

server = Server()
server.app.on_startup.append(server.setup)
server.app.on_cleanup.append(server.teardown)
web.run_app(server.app, port=VOL_PORT)
