import os
from typing import Union

import sqlalchemy as sa

from .schema import DB

PGUSER = os.environ.get('PGUSER') or 'lb'
PGPASSWD = os.environ.get('PGPASSWD') or 'lb'
PGHOST = os.environ.get('PGHOST') or '127.0.0.1'
PGPORT = os.environ.get('PGPORT') or 5432
DBNAME = os.environ.get('DBNAME') or 'cryptocompare'


class VolatilityNotFoundForTimestamp(Exception):
    pass


class DBClient:

    def __init__(self):
        self.engine = sa.create_engine(
            f'postgresql://{PGUSER}:{PGPASSWD}'
            f'@{PGHOST}:{PGPORT}/{DBNAME}'
        )
        self.db = DB()

    def setup_db(self) -> None:
        self.db.drop_tables(self.engine)
        self.db.create_tables(self.engine)
        self.db.set_permissions(PGUSER, self.engine)

    def close(self) -> None:
        self.engine.dispose()

    # INSERTS
    def insert(self, table: sa.Table,
               data: Union[list, dict]) -> sa.engine.ResultProxy:

        return self.engine.execute(
            table.insert().values(data)
        )

    def insert_log_returns(self, returns: list) -> sa.engine.ResultProxy:
        return self.insert(self.db.log_returns, returns)

    def insert_volatility(self, volatility:
                          Union[list, dict]) -> sa.engine.ResultProxy:
        return self.insert(self.db.volatilities, volatility)

    # SELECTS
    def get_volatility_by_timestamp(
            self, principal: str, collateral: str, timestamp: int) -> dict:
        """
        Return volatility row for this pair at timestamp
        Raises VolatilityNotFoundForTimestamp if data not present
        (because of failure or pending scrape)
        """
        volatilities = self.db.volatilities
        query = volatilities.select().where(
            sa.and_(
                volatilities.c.timestamp == timestamp,
                volatilities.c.principal == principal,
                volatilities.c.collateral == collateral,
            )
        )
        result = self.engine.execute(query).fetchone()

        if not result:
            raise VolatilityNotFoundForTimestamp()

        return dict(result)

    def get_log_returns_after(
            self, principal: str, collateral: str, timestamp: float) -> list:
        """
        Return log returns as list for this pair starting from timestamp
        """
        log_returns = self.db.log_returns
        query = log_returns.select().where(
            sa.and_(
                log_returns.c.timestamp >= timestamp,
                log_returns.c.principal == principal,
                log_returns.c.collateral == collateral
            )
        ).order_by(sa.asc(log_returns.c.timestamp))
        return [row['return'] for row in self.engine.execute(query)]

    # DELETES
    def clear_returns(self) -> sa.engine.ResultProxy:
        """
        Delete all log returns from table
        """
        return self.engine.execute(
            self.db.log_returns.delete()
        )

    def clear_vols(self) -> sa.engine.ResultProxy:
        """
        Delete all volatility rows from table
        """
        return self.engine.execute(
            self.db.volatilities.delete()
        )

    def clear_returns_before(self, timestamp: int) -> sa.engine.ResultProxy:
        """
        Delete all returns before the given timestamp
        """
        return self.engine.execute(
            self.db.log_returns.delete().where(
                self.db.log_returns.c.timestamp < timestamp
            )
        )
