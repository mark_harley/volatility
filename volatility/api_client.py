import aiohttp
import math

from typing import Union


class CryptoCompareClient:
    """
    Holds logic for getting data from CryptoCompare
    """

    def __init__(self, loop):
        self.loop = loop
        self.api_url = 'https://min-api.cryptocompare.com'
        self.session = aiohttp.ClientSession(loop=self.loop)

    async def close(self):
        await self.session.close()

    async def get(self, path: str,
                  params: Union[dict, None]=None) -> aiohttp.ClientResponse:
        return await self.session.get(
            f'{self.api_url}{path}',
            params=params
        )

    async def get_daily_price_data(
            self, principal_currency: str, collateral_currency: str,
            period: int, exchange: str='CCCAGG') -> list:
        """
        :param principal_currency: one of ('BTC','ETH', 'BCH', 'LTC')
        :param collateral_currency: one of ('BTC','ETH', 'BCH', 'LTC')
        :param period: number of days over which to take data
        CryptoCompare return a minimum of 2 days
        Default exchange is CCCAGG, which is actually an index,
        Crypto Coin Comparison Aggregated Index, as this is the
        easiest means to obtain data for all trading pairs. See,
        www.cryptocompare.com/media/12318004/cccagg.pdf
        """
        path = '/data/histoday'
        params = {
            'fsym': collateral_currency,
            'tsym': principal_currency,
            'e': exchange,
            'limit': period,
            'tryConversion': 'true',
        }
        resp = await self.get(path, params=params)
        async with resp:
            data = await resp.json()
            return data['Data']

    @staticmethod
    def calculate_log_returns(data: list) -> list:
        """
        Yield log returns from close prices in data
        data has keys 'time', the timestamp, and 'close', the price
        If no data is available (close = 0), return NaN for return
        """
        for i in range(1, len(data)):
            yday = data[i-1]
            today = data[i]
            try:
                yield {
                    'timestamp': today['time'],
                    'return': math.log(today['close']/yday['close']),
                }
            except ZeroDivisionError:
                yield {
                    'timestamp': today['time'],
                    'return': float('nan'),
                }

    async def get_log_returns(
            self, principal: str, collateral: str, days: int) -> list:

        prices = await self.get_daily_price_data(principal, collateral, days)
        return [
            {
                'timestamp': log_return['timestamp'],
                'return': log_return['return'],
                'principal': principal,
                'collateral': collateral,
            }
            for log_return in self.calculate_log_returns(prices)
        ]
