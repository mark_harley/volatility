from datetime import datetime


def utctoday_timestamp() -> float:
    """
    return timestamp for 00:00:00 today
    """
    return datetime.utcnow()\
        .replace(hour=0, minute=0, second=0, microsecond=0)\
        .timestamp()
