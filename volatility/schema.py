from sqlalchemy import Table, Column, Boolean, Integer, VARCHAR, MetaData
from sqlalchemy.dialects.postgresql import DOUBLE_PRECISION
from sqlalchemy.sql import text


class DB:
    def __init__(self):
        self.metadata = MetaData()

        self.log_returns = self.make_table(
            'log_returns',
            Column('return', DOUBLE_PRECISION, nullable=False),
        )

        self.volatilities = self.make_table(
            'volatilities',
            Column('month', DOUBLE_PRECISION, nullable=False),
            Column('three_months', DOUBLE_PRECISION, nullable=False),
            Column('year', DOUBLE_PRECISION, nullable=False),
            Column('three_month_inferred', Boolean, default=False),
            Column('year_inferred', Boolean, default=False),
        )

    def create_tables(self, engine):
        self.metadata.create_all(engine)

    def drop_tables(self, engine):
        self.metadata.drop_all(engine)

    def set_permissions(self, user, engine):
        for table in self.metadata.sorted_tables:
            engine.execute(
                text(f"GRANT ALL ON {table} TO {user}")
            )

    def make_table(self, name, *cols):
        return Table(
            name, self.metadata,
            Column('id', Integer, primary_key=True),
            Column('timestamp', DOUBLE_PRECISION, nullable=False),
            Column('principal', VARCHAR(3), nullable=False),
            Column('collateral', VARCHAR(3), nullable=False),
            *cols
        )
