import asyncio
from decimal import Decimal
import math
import random
from unittest.mock import Mock, MagicMock, call

from datetime import datetime, timedelta, timezone
import numpy as np
import pytest

from volatility import server
from volatility.utils import utctoday_timestamp


@pytest.fixture
def test_server(loop):
    return server.Server(loop)


def AsyncMock(*args, **kwargs):
    m = Mock(*args, **kwargs)

    async def mock_coro(*args, **kwargs):
        return m(*args, **kwargs)

    mock_coro.mock = m
    return mock_coro


def _calls_list_to_set(calls_list: list) -> set:
    return {c[1] for c in calls_list}


@pytest.fixture
def mock_compute_sd(monkeypatch):

    async def compute_sd(principal, collateral):
        return

    mock_compute = Mock(wraps=compute_sd)
    monkeypatch.setattr(server.Server, 'compute_vol_for_pair', mock_compute)
    return mock_compute


@pytest.fixture
def compute_volatilities_spy(test_server, monkeypatch):
    spy = Mock(wraps=test_server.compute_volatilities)
    monkeypatch.setattr(server.Server, 'compute_volatilities', spy)
    return spy


@pytest.fixture
def add_returns_to_db_spy(test_server, monkeypatch):
    spy = Mock(wraps=test_server.add_returns_to_db)
    monkeypatch.setattr(server.Server, 'add_returns_to_db', spy)
    return spy


@pytest.fixture
def one_year_ago():
    today = datetime.utcnow()\
            .replace(hour=0, minute=0, second=0, microsecond=0)
    return (today - timedelta(days=365)).timestamp()


def _generate_mock_returns(length: int) -> list:
    return [
        random.uniform(0, 1) for i in range(length)
    ]


def _generate_mock_returns_rows(
        principal: str, collateral: str,
        log_returns: list) -> list:
    return [
        {
            'timestamp': utctoday_timestamp() + i,
            'return': r,
            'principal': principal,
            'collateral': collateral,
        }
        for i, r in enumerate(log_returns)
    ]


class TestComputeVolForPair:

    @staticmethod
    def _get_sd(log_returns: list) -> tuple:
        year = np.std(log_returns)
        three_months = np.std(log_returns[-90:])
        month = np.std(log_returns[-30:])
        return month, three_months, year

    def _check_vol(self, db_test_client, expected):
        vol = db_test_client.get_volatility_by_timestamp(
            expected['principal'],
            expected['collateral'],
            expected['timestamp'],
        )
        for key in ('month', 'three_months', 'year',):
            assert math.isclose(vol[key], expected[key], rel_tol=1e-6)

        for key in (
                    'three_month_inferred', 'year_inferred',
                    'timestamp', 'principal', 'collateral'):
            assert vol[key] == expected[key]

    async def test_success(self, monkeypatch, db_test_client,
                           test_server, one_year_ago):
        principal = 'ETH'
        collateral = 'BTC'
        days = 365
        today = utctoday_timestamp()

        mock_returns = _generate_mock_returns(days)
        returns_rows = _generate_mock_returns_rows(
            principal, collateral, mock_returns
        )
        db_test_client.insert_log_returns(returns_rows)
        month, three_months, year = self._get_sd(mock_returns)

        await test_server.compute_vol_for_pair(principal, collateral)

        expected = {
            'id': 1,
            'principal': principal,
            'collateral': collateral,
            'month': round(Decimal(month), 15),
            'three_months': round(Decimal(three_months), 15),
            'year': round(Decimal(year), 15),
            'timestamp': today,
            'three_month_inferred': False,
            'year_inferred': False,
        }
        self._check_vol(db_test_client, expected)

    async def test_no_data_at_start_of_year(self, test_server, db_test_client):
        principal = 'a'
        collateral = 'b'
        days = 365

        mock_returns = [float('nan')] * 10 + _generate_mock_returns(days - 10)
        returns_rows = _generate_mock_returns_rows(
            principal, collateral, mock_returns
        )
        db_test_client.insert_log_returns(returns_rows)

        three_months = np.std(mock_returns[-90:])
        month = np.std(mock_returns[-30:])

        # Annualised vol
        year = math.sqrt(12)*month

        await test_server.compute_vol_for_pair(principal, collateral)

        expected = {
            'principal': principal,
            'collateral': collateral,
            'month': month,
            'three_months': three_months,
            'year': year,
            'timestamp': utctoday_timestamp(),
            'three_month_inferred': False,
            'year_inferred': True,
        }
        self._check_vol(db_test_client, expected)

    async def test_no_data_within_three_months(
                            self, test_server, db_test_client):
        principal = 'a'
        collateral = 'b'
        days = 365

        mock_returns = (
            [float('nan')] * 310 + _generate_mock_returns(days - 310)
        )
        returns_rows = _generate_mock_returns_rows(
            principal, collateral, mock_returns
        )
        db_test_client.insert_log_returns(returns_rows)

        month = np.std(mock_returns[-30:])

        # Implied vol
        three_months = math.sqrt(3) * month
        year = math.sqrt(12) * month

        await test_server.compute_vol_for_pair(principal, collateral)

        expected = {
            'principal': principal,
            'collateral': collateral,
            'month': month,
            'three_months': three_months,
            'year': year,
            'timestamp': utctoday_timestamp(),
            'three_month_inferred': True,
            'year_inferred': True,
        }
        self._check_vol(db_test_client, expected)

    async def test_insufficient_data_for_vol_calc(
                            self, test_server, db_test_client):
        principal = 'a'
        collateral = 'b'
        days = 365

        mock_returns = (
            [float('nan')] * 340 + _generate_mock_returns(days - 340)
        )
        returns_rows = _generate_mock_returns_rows(
            principal, collateral, mock_returns
        )
        db_test_client.insert_log_returns(returns_rows)

        with pytest.raises(Exception) as ex_info:
            await test_server.compute_vol_for_pair(principal, collateral),

        assert 'Insufficient data' == str(ex_info.value)
        assert isinstance(ex_info.value, Exception)


class TestGetDailyData:

    @pytest.fixture
    def mock_sleep(self, monkeypatch):
        mock_sleep = AsyncMock()
        monkeypatch.setattr(server.asyncio, 'sleep', mock_sleep)
        return mock_sleep

    @pytest.fixture
    def freeze_time(self, monkeypatch):
        mock_utcnow = Mock(
            return_value=datetime(
                2018, 6, 19, 23, 0, 0, tzinfo=timezone.utc)
            )
        mock_datetime = MagicMock()
        mock_datetime.utcnow = mock_utcnow
        monkeypatch.setattr(server, 'datetime', mock_datetime)
        return mock_datetime

    @pytest.fixture
    def mock_create_task(self, loop):
        loop.create_task = Mock()
        return loop.create_task

    async def test_get_daily_data(self, test_server, mock_sleep,
                                  one_year_ago, freeze_time, mock_compute_sd):
        await test_server.get_daily_data()

        mock_sleep.mock.assert_called_once_with(3601.0)
        assert (
            mock_compute_sd.mock_calls ==
            [call(p, c) for p, c in test_server.trading_pairs]
        )
        assert test_server.get_daily_data_task is not None

        test_server.get_daily_data_task.set_result('done')
        test_server.get_daily_data_task.cancel()
        await test_server.get_daily_data_task


class TestSetup:

    @pytest.fixture
    def add_garbage_returns(self, test_server, db_test_client):
        garbage = _generate_mock_returns_rows(
            'BTC', 'ETH',
            _generate_mock_returns(1000),
        )
        db_test_client.insert_log_returns(garbage)

    async def test_setup(self, test_server, mock_compute_sd,
                         add_returns_to_db_spy, compute_volatilities_spy):
        await test_server.setup(test_server.app)

        add_returns_to_db_spy.assert_called_once_with(365)
        compute_volatilities_spy.assert_called_once()

        assert (
            mock_compute_sd.mock_calls ==
            [call(p, c) for p, c in test_server.trading_pairs]
        )

    async def test_returns_added_for_all_pairs(
                self, test_server, db_test_client,
                mock_compute_sd, add_garbage_returns):

        await test_server.setup(test_server.app)

        # Check old rows are cleared and correct number added
        days = 365
        for p, c in test_server.trading_pairs:
            returns = db_test_client.get_log_returns_after(p, c, 0)
            assert len(returns) == days


class TestTearDown:

    @pytest.fixture
    def mock_api_close(self, test_server):
        test_server.api_client.close = AsyncMock()

    @pytest.fixture
    def db_close_spy(self, monkeypatch, test_server):
        spy = Mock(wraps=test_server.db.close)
        monkeypatch.setattr(server.db_client.DBClient, 'close', spy)
        return spy

    async def test_teardown(self, test_server, mock_api_close, db_close_spy):
        future = asyncio.Future()
        future.set_result('done')
        future.cancel = Mock(wraps=future.cancel)
        test_server.get_daily_data_task = future

        await test_server.teardown(test_server.app)

        future.cancel.assert_called_once()
        test_server.api_client.close.mock.assert_called_once()
        db_close_spy.assert_called_once()
