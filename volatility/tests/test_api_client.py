import math
import pytest
import random

import numpy as np


class TestGetLogReturns:

    @pytest.fixture
    def add_missing_data(self, api_test_client):
        data = [{'time': 0, 'close': 0}]
        for i in range(9):
            data.append({'time': i, 'close': random.uniform(0, 1)})
        api_test_client.session.set_next_call(data)

    async def test_get_log_returns(self, api_test_client, mock_api_session):
        principal = 'BTC'
        collateral = 'ETH'
        days = 10
        result = await api_test_client.get_log_returns(
            principal, collateral, days
        )

        data = api_test_client.session.mock_data[0]['Data']
        returns = [a['close'] for a in data]
        log_returns = []
        for i in range(1, len(data)):
            log_returns.append(math.log(returns[i]/returns[i-1]))
        assert log_returns == [res['return'] for res in result]

    async def test_get_returns_with_missing_data(
            self, api_test_client, mock_api_session, add_missing_data):
        principal = 'BTC'
        collateral = 'ETH'
        days = 10
        result = await api_test_client.get_log_returns(
            principal, collateral, days
        )

        data = api_test_client.session.mock_data[0]['Data']
        returns = [a['close'] for a in data]
        log_returns = []
        for i in range(2, len(data)):
            log_returns.append(math.log(returns[i]/returns[i-1]))
        assert log_returns == [res['return'] for res in result][1:]
        assert np.isnan(result[0]['return'])
