import math
import pytest
import random

from volatility.server import Server
from volatility.utils import utctoday_timestamp


@pytest.fixture
def populate_test_db(db_test_client):
    vols = [
        {
            'principal': p,
            'collateral': c,
            'month': random.uniform(0, 1),
            'three_months': random.uniform(0, 1),
            'year': random.uniform(0, 1),
            'timestamp': utctoday_timestamp(),
            'three_month_inferred': False,
            'year_inferred': True if p == 'BCH' else False,
        }
        for p, c in Server.trading_pairs
    ]
    db_test_client.insert_volatility(vols)
    return vols


@pytest.fixture
def populate_test_db_yday(db_test_client):
    vols = [
        {
            'principal': p,
            'collateral': c,
            'month': random.uniform(0, 1),
            'three_months': random.uniform(0, 1),
            'year': random.uniform(0, 1),
            'timestamp': utctoday_timestamp() - 1,
            'three_month_inferred': False,
            'year_inferred': True if p == 'BCH' else False,
        }
        for p, c in Server.trading_pairs
    ]
    db_test_client.insert_volatility(vols)
    return vols


@pytest.fixture
def test_client(loop, aiohttp_client):
    server = Server()
    app = server.app
    app.router.add_get('/volatility', server.handle_get)
    return loop.run_until_complete(aiohttp_client(app))


async def make_request(test_client, principal, collateral):
    return await test_client.get(
        '/volatility',
        params={
            'principal': principal,
            'collateral': collateral,
        },
    )


async def test_get_volatility(populate_test_db, test_client):
    for vol in populate_test_db:
        response = await make_request(
            test_client,
            vol['principal'],
            vol['collateral']
        )
        async with response:
            assert response.status == 200
            body = await response.json()
            for key in ('month', 'three_months', 'year',):
                assert math.isclose(body[key], vol[key], rel_tol=1e-6)


async def test_symbol_not_in_list(populate_test_db, test_client):
    response = await make_request(test_client, 'EOS', 'BTC')

    async with response:
        assert response.status == 404
        body = await response.json()
        assert body['error'] == "Currency pair not currently supported"


async def test_todays_vol_not_ready(populate_test_db_yday, test_client):
    response = await make_request(test_client, 'BCH', 'BTC')

    async with response:
        assert response.status == 420
        body = await response.json()
        assert body['error'] == ("Today's volatility pending for BCH/BTC."
                                 " Try again later")


async def test_missing_parameter(test_client):
    response = await test_client.get(
        '/volatility',
        params={
            'principal': 'BTC',
        },
    )

    async with response:
        assert response.status == 400
        body = await response.json()
        assert body['error'] == "missing parameter 'collateral'"
