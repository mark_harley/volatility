import pytest
import random

import sqlalchemy as sa

from volatility import server

NUMBER_TEST_ROWS = 365


@pytest.fixture
def populate_test_returns(db_test_client):
    data = []
    for p, c in server.Server.trading_pairs:
        for i in range(NUMBER_TEST_ROWS):
            data.append(
                {
                    'timestamp': float(i),
                    'principal': p,
                    'collateral': c,
                    'return': random.uniform(0, 1),
                }
            )
    db_test_client.engine.execute(
        db_test_client.db.log_returns.insert(data)
    )


class TestGetLogReturnsAfter:

    def test_get_log_returns_after(
            self, db_test_client, populate_test_returns):
        principal = 'BTC'
        collateral = 'ETH'
        min_timestamp = 100
        returns = db_test_client.get_log_returns_after(
            principal, collateral, min_timestamp
        )

        log_returns = db_test_client.db.log_returns
        min_timestamp_row = db_test_client.engine.execute(
            log_returns.select().where(
                sa.and_(
                    log_returns.c.timestamp == min_timestamp,
                    log_returns.c.principal == principal,
                    log_returns.c.collateral == collateral
                )
            )
        ).fetchone()

        assert len(returns) == NUMBER_TEST_ROWS - min_timestamp
        assert min_timestamp_row['return'] == returns[0]
