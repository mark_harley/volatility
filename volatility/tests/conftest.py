import os
import pytest
import random

from volatility import api_client
from volatility import db_client
from volatility import server


# In the real world this is obviously a bad idea
# Certainly shouldn't be running these on the live
# servers anyway!
PGUSER_TEST = os.environ.get('PGUSER_TEST') or 'lb_test'
PGPASSWD_TEST = os.environ.get('PGPASSWD_TEST') or 'lb_test'
DBNAME_TEST = os.environ.get('DBNAME_TEST') or 'lb_test'


@pytest.fixture(autouse=True)
def mock_db_info(monkeypatch):
    monkeypatch.setattr(db_client, 'PGUSER', PGUSER_TEST)
    monkeypatch.setattr(db_client, 'PGPASSWD', PGPASSWD_TEST)
    monkeypatch.setattr(db_client, 'DBNAME', DBNAME_TEST)


@pytest.fixture()
def db_test_client(mock_db_info):
    return db_client.DBClient()


@pytest.fixture(autouse=True)
def setup_test_db(db_test_client):
    db_test_client.setup_db()


@pytest.fixture(autouse=True)
def teardown_test_db(db_test_client):
    yield
    db_test_client.db.drop_tables(db_test_client.engine)
    db_test_client.close()


class MockResponse:
    def __init__(self, data):
        self.data = data

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        return

    async def json(self):
        return self.data


class MockApi:
    def __init__(self, loop):
        self.mock_data = []
        self.mock_calls = []
        self.next_calls = []
        self.loop = loop

    def set_next_call(self, data):
        self.next_calls.append({'Data': data})

    async def get(self, url, params):
        self.mock_calls.append(
            (params['tsym'], params['fsym'], params['limit'])
        )
        if self.next_calls:
            data = self.next_calls.pop()
        else:
            data = {'Data': [
                {'time': i, 'close': random.uniform(1, 1000)}
                for i in range(params['limit']+1)
            ]}

        self.mock_data.append(data)
        return MockResponse(data)


@pytest.fixture(autouse=True)
def mock_api_session(monkeypatch):
    mock = MockApi
    monkeypatch.setattr(api_client.aiohttp, 'ClientSession', mock)
    monkeypatch.setattr(server.api_client.aiohttp, 'ClientSession', mock)
    return mock


@pytest.fixture
def api_test_client(loop):
    return api_client.CryptoCompareClient(loop)
