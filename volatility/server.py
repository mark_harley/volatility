import asyncio
import math
import os

from aiohttp import web
from datetime import datetime, timedelta
from itertools import permutations
import numpy as np

from volatility import api_client, db_client
from .utils import utctoday_timestamp

DEBUG = os.environ.get('VOL_DEBUG') or False


class Server:
    currencies = ('BTC', 'ETH', 'BCH', 'LTC',)
    trading_pairs = list(permutations(currencies, 2))

    def __init__(self, loop=None):
        if not loop:
            loop = asyncio.get_event_loop()
        self.loop = loop
        self.loop.set_debug(DEBUG)
        self.app = web.Application(loop=self.loop)
        self.api_client = api_client.CryptoCompareClient(self.loop)
        self.db = db_client.DBClient()
        self.get_daily_data_task = None
        self.startup_scrape_pending = False

    async def setup(self, app: web.Application) -> None:
        """
        Run by aiohttp on startup
        - Clears the database and recreates tables
        - Grabs a year of returns from CryptoCompare
        - Configures the aiohttp routes
        - Creates the daily scrape task
        """
        self.db.setup_db()
        await self.startup_scrape()
        self.setup_routes()
        self.get_daily_data_task = self.loop.create_task(
            self.get_daily_data()
        )

    async def teardown(self, app: web.Application) -> None:
        """
        Run by aiohttp on shutdown
        - Cancels the daily scrape task
        - Closes the api_client session
        - Closes the database connection
        """
        task = self.get_daily_data_task
        if task:
            task.cancel()
            await task
        await self.api_client.close()
        self.db.close()

    def setup_routes(self) -> None:
        self.app.router.add_get('/volatility', self.handle_get)

    async def handle_get(self, request: web.Request) -> web.Response:
        """
        API endpoint to handle requests for volatility
        """
        principal = request.query.get('principal')
        collateral = request.query.get('collateral')

        if not principal:
            return self._missing_param_response('principal')

        if not collateral:
            return self._missing_param_response('collateral')

        if (principal not in self.currencies
                or collateral not in self.currencies):
            return self._error_response(
                "Currency pair not currently supported",
                status=404,
            )

        try:
            vol = self.db.get_volatility_by_timestamp(
                principal, collateral, utctoday_timestamp()
            )
        except db_client.VolatilityNotFoundForTimestamp:
            return self._error_response(
                f"Today's volatility pending for {principal}/{collateral}."
                " Try again later",
                status=420,
            )

        return web.json_response(vol)

    def _missing_param_response(self, param: str) -> web.Response:
        return self._error_response(
            f"missing parameter '{param}'",
            status=400,
        )

    @staticmethod
    def _error_response(message: str, status: int) -> web.Response:
        return web.json_response(
            {'error': message},
            status=status,
        )

    async def startup_scrape(self) -> None:
        """
        - Clears the database (not the most efficient solution but certainly
        the simplest)
        - Requests 365 days of price data for all trading pairs
        - Computes volatility and adds db records
        """
        self.db.clear_returns()
        self.db.clear_vols()
        await self.add_returns_to_db(365)
        await self.compute_volatilities()

    async def get_daily_data(self) -> None:
        """
        Daily task, runs at 00:00:01 each day
        - Gets day's close price and computes volatility
        """
        next_midnight = datetime.utcnow()\
            .replace(hour=0, minute=0, second=0) + timedelta(1)
        delay = (next_midnight - datetime.utcnow()).total_seconds() + 1
        print(f'Delaying for {delay}')
        await asyncio.sleep(delay)

        print('Running now')
        self.clear_old_returns()
        await self.add_returns_to_db(days=1)
        await self.compute_volatilities()
        self.get_daily_data_task = self.loop.create_task(
            self.get_daily_data()
        )
        print('Done')

    def clear_old_returns(self) -> None:
        """
        Remove returns from the table that fall outwith the
        preceding 365 days
        """
        year_in_seconds = 365 * 24 * 60 * 60
        oldest_needed = utctoday_timestamp() - year_in_seconds
        self.db.clear_returns_before(oldest_needed)

    async def add_returns_to_db(self, days: int) -> None:
        """
        For each pair schedule the task which obtains the
        price data over number of `days` and inserts returns into DB
        """
        get_return_coros = (
            self.get_returns_for_pair(principal, collateral, days)
            for principal, collateral in self.trading_pairs
        )
        await asyncio.gather(*get_return_coros)

    async def get_returns_for_pair(
            self, principal: str, collateral: str, days: int) -> None:
        """
        Get price data for this pair, compute log returns and insert
        into DB
        """
        data = await self.api_client\
                         .get_log_returns(
                             principal, collateral, days
                         )
        self.db.insert_log_returns(data)

    async def compute_volatilities(self) -> None:
        """
        Schedule the tasks to compute volatility for each pair
        """
        print('Computing vols')
        compute_coros = [
            self.compute_vol_for_pair(principal, collateral)
            for principal, collateral in self.trading_pairs
        ]
        await asyncio.gather(*compute_coros)

    async def compute_vol_for_pair(
            self, principal: str, collateral: str) -> None:
        """
        Compute the volatility for this pair and insert into DB.
        If data was not present for this pair then we compute
        annualised/three-monthly-ised volatility by multiplying the
        monthly by sqrt(12) or sqrt(3) respectively
        """
        print(f'Computing vol for {principal}/{collateral}')
        timestamp = utctoday_timestamp() - 365*24*60*60
        data = self.db.get_log_returns_after(principal, collateral, timestamp)

        month = data[-30:]
        three_months = data[-90:]

        three_month_inferred = False
        year_inferred = False

        if any(np.isnan(month)):
            raise Exception('Insufficient data')

        sd_month = np.std(month)

        # If data missing, approximate volatility from monthly
        if any(np.isnan(three_months)):
            sd_three_months = math.sqrt(3) * sd_month
            sd_year = math.sqrt(12) * sd_month

            three_month_inferred = True
            year_inferred = True

        elif any(np.isnan(data)):
            sd_three_months = np.std(three_months)
            sd_year = math.sqrt(12) * sd_month

            year_inferred = True

        else:
            sd_three_months = np.std(three_months)
            sd_year = np.std(data)

        self.db.insert_volatility({
            'principal': principal,
            'collateral': collateral,
            'month': sd_month,
            'three_months': sd_three_months,
            'year': sd_year,
            'timestamp': utctoday_timestamp(),
            'three_month_inferred': three_month_inferred,
            'year_inferred': year_inferred,
        })
        print(f'Done {principal}/{collateral}')
